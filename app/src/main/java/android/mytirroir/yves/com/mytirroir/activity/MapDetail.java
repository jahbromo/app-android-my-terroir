package android.mytirroir.yves.com.mytirroir.activity;

import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.fragment.TerroirMapDetailFragment;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class MapDetail extends AppCompatActivity {

    private TerroirMapDetailFragment mapFragment;
    private DataEvent dataEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.map_detail_activity);
        dataEvent = getIntent().getParcelableExtra(Constant.KEY_TERROIR);
        mapFragment = TerroirMapDetailFragment.newInstance(dataEvent);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.activity_map_layout,mapFragment).commit();
        setTitle(dataEvent.getSelected().getName());
        ((TextView)findViewById(R.id.map_detail_title)).setText(dataEvent.getSelected().getName());
    }

    public void closeView(View view) {
        finish();
    }

    public void launchDirection(View view) {
        Constant.launchDirection(this,dataEvent.getSelected());
    }
}
