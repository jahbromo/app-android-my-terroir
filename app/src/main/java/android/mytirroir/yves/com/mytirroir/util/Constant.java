package android.mytirroir.yves.com.mytirroir.util;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.activity.Detail;
import android.mytirroir.yves.com.mytirroir.activity.MapDetail;
import android.mytirroir.yves.com.mytirroir.activity.MapList;
import android.mytirroir.yves.com.mytirroir.domain.Category;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.event.TerroirSelectedEvent;
import android.net.Uri;

import com.google.maps.android.ui.IconGenerator;

public class Constant {

    public static final String KEY_TERROIR = "data";
    public static final String KEY_SELECTED = "selected_terroir";
    public static final String KEY_DATA = "terroir";
    public static final String TEXT = "Paris est tres connue sous forme de capital...et aussie sa mgnifique tour effeil de 324m de haut!!!maaais surtout les parisien et parisienne on de la chance davoir sa devant leur fenetre au reveille...";
    public static final String KEY_DEPARTEMENTS = "departements";


    public static void launchDetail(Context context, DataEvent dataEvent) {
        Intent intent = new Intent(context, MapList.class);
        intent.putExtra(KEY_TERROIR, dataEvent);
        context.startActivity(intent);
    }

    public static void launchDetailInfo(Context context, Terroir terroir) {
        Intent intent = new Intent(context, Detail.class);
        intent.putExtra(KEY_DATA, terroir);
        context.startActivity(intent);
    }



    public static int getCategoryColor(Category category) {
        if (category == Category.ARTISANAT) {
            return R.color.color_artisanat;
        }
        if (category == Category.EVENEMENT) {
            return R.color.color_evenement;
        }
        if (category == Category.PATRIMOINE) {
            return R.color.color_patrimoine;
        }
        if (category == Category.NATURE) {
            return R.color.color_nature;
        }
        if (category == Category.GASTRONOMIE) {
            return R.color.color_gastronomie;
        }
        return Color.RED;
    }
    public  static  void launchDirection(Context context,Terroir terroir){
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + terroir.getLatitude() + "," + terroir.getLongitude());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mapIntent);
    }
    public  static  void share(Context context,Terroir terroir){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+terroir.getLatitude()+","+terroir.getLongitude());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mapIntent);
    }

    public static void launchInfo(Context context, Terroir terroir) {
        Intent intent = new Intent(context, Detail.class);
        intent.putExtra(KEY_DATA, terroir);
        context.startActivity(intent);
    }

    public static void launchShare(Context context, Terroir terroir) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, terroir.getName() + " " + terroir.getDetail());
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(Intent.createChooser(sendIntent, "Share Terroir"));
    }


    public static void launchPoi(Context context, DataEvent dataEvent) {
        Intent intent = new Intent(context, Detail.class);
        intent.putExtra(Constant.KEY_SELECTED, dataEvent);
        context.startActivity(intent);
    }

    public static void launchMapPoi(Context context, DataEvent dataEvent) {
        Intent intent = new Intent(context, MapList.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(KEY_TERROIR, dataEvent);
        context.startActivity(intent);
    }

    public static void launchMapDetail(Context context, DataEvent dataEvent) {
        Intent intent = new Intent(context, MapDetail.class);
        intent.putExtra(KEY_TERROIR, dataEvent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}

