package android.mytirroir.yves.com.mytirroir.domain;


import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Terroir implements Parcelable {

    private int id;
    private String name;
    private String category;
    private String sub_category;
    private String detail;
    private String photo_id;
    private String photo_list;
    private String photo_detail;
    private double latitude;
    private double longitude;
    private double altitude;
    private String departement;
    private String code;
    private String adresse;
    private String city;
    private String tel;
    private String horaire;
    private String website;
    private String wikipedia;

    private double distance;
    private int logo;
    private int selectLogo;

    private String H = "Mardi  12h00 - 14h   20h - 22h";
           ;

    public int getId() {
        return id;
    }

    public void setIsFavorite(boolean isFavorite) {

    }

    public Category getCategory() {

        if (this.category.toLowerCase().startsWith("nature")) {
            return Category.NATURE;
        }
        if (this.category.toLowerCase().startsWith("arti")) {
            return Category.ARTISANAT;
        }
        if (this.category.toLowerCase().startsWith("patrimoine")) {
            return Category.PATRIMOINE;
        }
        if (this.category.toLowerCase().startsWith("gastro")) {
            return Category.GASTRONOMIE;
        }
        if (this.category.toLowerCase().startsWith("evene")) {
            return Category.EVENEMENT;
        }
        return Category.ALL;
    }

    public String getName() {
        return name;
    }

    public String getSub_category() {
        return sub_category;
    }

    public String getDetail() {
        return detail;
    }

    public String getPhoto_id() {
        return photo_id;
    }

    public String getPhoto_list() {
        return photo_list;
    }

    public String getPhoto_detail() {
        return photo_detail;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public String getDepartement() {
        return departement;
    }

    public String getCode() {
        return code;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getCity() {
        return city;
    }

    public String getTel() {
        return tel;
    }

    public String getHoraire() {
        return H;
    }

    public String getWebsite() {
        return website;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public boolean isFavorite() {
        return true;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.category);
        dest.writeString(this.sub_category);
        dest.writeString(this.detail);
        dest.writeString(this.photo_id);
        dest.writeString(this.photo_list);
        dest.writeString(this.photo_detail);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.altitude);
        dest.writeString(this.departement);
        dest.writeString(this.code);
        dest.writeString(this.adresse);
        dest.writeString(this.city);
        dest.writeString(this.tel);
        dest.writeString(this.horaire);
        dest.writeString(this.website);
        dest.writeString(this.wikipedia);
    }

    public int getColor() {
        if (getCategory() == Category.EVENEMENT) {
            return R.color.color_evenement;
        }
        if (getCategory() == Category.NATURE) {
            return R.color.color_nature;

        }
        if (getCategory() == Category.PATRIMOINE) {
            return R.color.color_patrimoine;
        }
        if (getCategory() == Category.GASTRONOMIE) {
            return R.color.color_gastronomie;
        }
        if (getCategory() == Category.ARTISANAT) {
            return R.color.color_artisanat;
        }
        return R.color.title_color;

    }

    public Terroir() {
    }

    protected Terroir(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.category = in.readString();
        this.sub_category = in.readString();
        this.detail = in.readString();
        this.photo_id = in.readString();
        this.photo_list = in.readString();
        this.photo_detail = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.altitude = in.readDouble();
        this.departement = in.readString();
        this.code = in.readString();
        this.adresse = in.readString();
        this.city = in.readString();
        this.tel = in.readString();
        this.horaire = in.readString();
        this.website = in.readString();
        this.wikipedia = in.readString();
    }

    public String getIcon() {
        if (category == null) {
            return "X";
        }
        return " "+this.category.substring(0, 1)+ " ";
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public static final Parcelable.Creator<Terroir> CREATOR = new Parcelable.Creator<Terroir>() {
        public Terroir createFromParcel(Parcel source) {
            return new Terroir(source);
        }

        public Terroir[] newArray(int size) {
            return new Terroir[size];
        }
    };

    @Override
    public String toString() {
        return "Terroir{" +
                "distance=" + distance +
                '}';
    }

    public List<Contact> detailItems() {
        List<Contact> detailItems = new ArrayList<>();
        detailItems.add(new Contact(R.drawable.ic_schedule_black_24dp, this.getHoraire(),0));
        detailItems.add(new Contact(R.drawable.ic_call_black_24dp, this.tel,1));
        detailItems.add(new Contact(R.drawable.ic_map_black_24dp, this.getAdresse(),2));
        detailItems.add(new Contact(R.drawable.ic_language_black_24dp, this.getWebsite(), 3));
        detailItems.add(new Contact(R.drawable.ic_wikipedia_logo_v2, this.getWikipedia(),3));
        return detailItems;
    }

    public List<Comment> comments() {
        List<Comment> comments = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            comments.add(Comment.create());
        }
        return comments;
    }

    public boolean isMatched(String query) {
        if(name.toLowerCase().contains(query.toLowerCase()) ){
            return  true;

        }
        if(sub_category.toLowerCase().contains(query.toLowerCase()) ){
            return  true;

        }
        if(adresse.toLowerCase().contains(query.toLowerCase()) ){
            return  true;

        }
        if(category.toLowerCase().contains(query.toLowerCase()) ){
            return  true;

        }

        if (departement.toLowerCase().contains(query.toLowerCase())) {
            return true;
        }
        if(city.toLowerCase().contains(query.toLowerCase()) ){
            return true;
        }
        return false;
    }

    public int getLogo() {
        if (getCategory() == Category.EVENEMENT) {
            return R.drawable.evenement;
        }
        if (getCategory() == Category.NATURE) {
            return R.drawable.nature;

        }
        if (getCategory() == Category.PATRIMOINE) {
            return R.drawable.patrimoine;
        }
        if (getCategory() == Category.GASTRONOMIE) {
            return R.drawable.gatro;
        }
        if (getCategory() == Category.ARTISANAT) {
            return R.drawable.artisanat;
        }
        return R.drawable.localisation;
    }

    public int getSelectLogo() {
        if (getCategory() == Category.EVENEMENT) {
            return R.drawable.pin_e;
        }
        if (getCategory() == Category.NATURE) {
            return R.drawable.pin_n;

        }
        if (getCategory() == Category.PATRIMOINE) {
            return R.drawable.pin_p;
        }
        if (getCategory() == Category.GASTRONOMIE) {
            return R.drawable.pin_g;
        }
        if (getCategory() == Category.ARTISANAT) {
            return R.drawable.pin_a;
        }
        return R.drawable.pin_l;
    }
    public boolean isInDepartement(String code){
        return  this.code.toLowerCase().startsWith(code.toLowerCase());
    }
}

