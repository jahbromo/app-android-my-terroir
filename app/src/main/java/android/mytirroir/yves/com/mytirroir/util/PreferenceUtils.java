package android.mytirroir.yves.com.mytirroir.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.widget.CompoundButton;

public class PreferenceUtils {


    public static DepartementEvent lastDepartementCode(Context context) {
        SharedPreferences settings = context.getSharedPreferences("DEPART", 0);
        Departement departement = new Departement();
        departement.setId(settings.getInt("code", -1));
        departement.setName(settings.getString("name", null));
        departement.setPrefecture(settings.getString("prefecture", null));
        departement.setRegion(settings.getString("region", null));
        departement.setImage(settings.getString("image", null));
        Location location = new Location("departement");
        location.setLatitude(Double.parseDouble(settings.getString("latitude", "-1")));
        location.setLongitude(Double.parseDouble(settings.getString("longitude","-1")));
        if(departement.getId()<0){
            return Departement.getDefault();
        }
        if(location.getLongitude() ==1){
            location = null;
        }
        return  new DepartementEvent(departement,location);
    }

    public static void saveCodeDepartement(Context context, DepartementEvent event) {
        Departement terroir = event.getDepartement();
        Location location = event.getLocation();
        SharedPreferences settings = context.getSharedPreferences("DEPART", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("code", terroir.getId());
        editor.putString("name", String.valueOf(terroir.getName()));
        editor.putString("region", String.valueOf(terroir.getRegion()));
        editor.putString("prefecture", String.valueOf(terroir.getPrefecture()));
        if(location!=null){
            editor.putString("latitude", String.valueOf(location.getLatitude()));
            editor.putString("longitude", String.valueOf(location.getLongitude()));
        }
        editor.commit();
    }
}
