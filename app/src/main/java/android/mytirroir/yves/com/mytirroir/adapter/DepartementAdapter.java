package android.mytirroir.yves.com.mytirroir.adapter;

import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.view.ContactItemView;
import android.mytirroir.yves.com.mytirroir.view.DepartementItemView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class DepartementAdapter extends BaseAdapter {

    List<Departement> detailItems = new ArrayList<>();

    public DepartementAdapter(List<Departement> detailItems) {
        this.detailItems = detailItems;
    }
    public DepartementAdapter(Departement departement) {
        this.detailItems = new ArrayList<>();
        this.detailItems.add(departement);
    }


    public DepartementAdapter() {
    }

    @Override
    public int getCount() {
        return detailItems.size();
    }

    @Override
    public Object getItem(int position) {
        return detailItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Departement detailItem = detailItems.get(position);
        if (convertView != null && convertView instanceof DepartementItemView) {
            DepartementItemView itemView = (DepartementItemView) convertView;
            itemView.reuse(detailItem);
            return itemView;
        }
        return new DepartementItemView(parent.getContext(), detailItem);
    }
}
