package android.mytirroir.yves.com.mytirroir.activity;

import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.view.DetailHeaderView;
import android.mytirroir.yves.com.mytirroir.view.DetailInfoView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.WindowCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class Detail extends AppCompatActivity {

    private DataEvent dataEvent;
    private DetailHeaderView detailHeaderView;
    private DetailInfoView detailInfoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(WindowCompat.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.detail_activivity_layout);
        dataEvent = getIntent().getParcelableExtra(Constant.KEY_SELECTED);
        setTitle(dataEvent.getSelected().getName());
        detailHeaderView = ((DetailHeaderView) findViewById(R.id.detail_header_view));
        detailInfoView = ((DetailInfoView) findViewById(R.id.detail_header_info));
        detailHeaderView.reuse(dataEvent.getSelected());
        detailInfoView.reuse(dataEvent.getSelected());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.getBackground().setAlpha(0);
        toolbar.setNavigationIcon(R.drawable.menu_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_direction) {
            Constant.launchMapDetail(getApplicationContext(),dataEvent);
            return true;
        }
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
    }
        return super.onOptionsItemSelected(item);
    }
}
