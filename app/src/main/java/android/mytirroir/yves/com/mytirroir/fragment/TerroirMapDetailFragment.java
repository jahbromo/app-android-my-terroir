package android.mytirroir.yves.com.mytirroir.fragment;

import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.activity.Detail;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.view.TirroirItemInfoView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;


public class TerroirMapDetailFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TirroirItemInfoView tirroirItemInfoView;
    private DataEvent dataEvent;

    public   static TerroirMapDetailFragment newInstance(DataEvent dataEvent){
        TerroirMapDetailFragment fragment = new TerroirMapDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.KEY_TERROIR, dataEvent);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_list_fragment, container, false);
        tirroirItemInfoView = (TirroirItemInfoView) v.findViewById(R.id.view_info);
        tirroirItemInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();

            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dataEvent = getArguments().getParcelable(Constant.KEY_TERROIR);
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(dataEvent.getMapOptions());
        FragmentManager fm = getChildFragmentManager();
        fm.beginTransaction().replace(R.id.map_layout, supportMapFragment).commit();
        supportMapFragment.getMapAsync(this);
    }

    public void setSelectedTerroir(Terroir selectedTerroir){
        dataEvent.setSelected(selectedTerroir);
        addIcon(selectedTerroir);
        centerOnMarker(selectedTerroir);
        EventBus.getDefault().post(new TitleEvent(selectedTerroir.getName()));
    }
    public void centerOnMarker(final Terroir terroir){
        tirroirItemInfoView.setVisibility(View.VISIBLE);
        tirroirItemInfoView.setTerroir(terroir);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(terroir.getLatLng(), 10.0f));
        mMap.setPadding(100, 150, 70, 170);
    }

    private Marker addIcon(Terroir terroir) {
        MarkerOptions markerOptions = new MarkerOptions().snippet(terroir.getName()).
                icon(BitmapDescriptorFactory.fromResource(terroir.getSelectLogo())).
                position(terroir.getLatLng());
        return mMap.addMarker(markerOptions);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        setSelectedTerroir(dataEvent.getSelected());
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                setSelectedTerroir(dataEvent.getSelected());
                try{
                    mMap.setMyLocationEnabled(true);
                }catch (Exception e){

                }
            }
        });
    }
}
