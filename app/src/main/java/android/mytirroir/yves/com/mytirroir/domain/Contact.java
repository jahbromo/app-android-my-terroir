package android.mytirroir.yves.com.mytirroir.domain;

public class Contact {

    private int icon;
    private String terroir;
    private int action;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTerroir() {
        return terroir;
    }

    public void setTerroir(String terroir) {
        this.terroir = terroir;
    }

    public Contact(int icon, String terroir,int action) {
        this.icon = icon;
        this.terroir = terroir;
        this.action = action;

    }

    public int getAction() {
        return action;
    }
}
