package android.mytirroir.yves.com.mytirroir.adapter;

import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.view.ContactItemView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends BaseAdapter {

    public ContactAdapter(Terroir terroir) {
        this.detailItems = terroir.detailItems();
    }

    private List<Contact> detailItems = new ArrayList<>();

    @Override
    public int getCount() {
        return detailItems.size();
    }

    @Override
    public Object getItem(int position) {
        return detailItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contact detailItem = detailItems.get(position);
        if (convertView != null && convertView instanceof ContactItemView) {
            ContactItemView itemView = (ContactItemView) convertView;
            itemView.reuse(detailItem);
            return itemView;
        }
        return new ContactItemView(parent.getContext(), detailItem);
    }
}
