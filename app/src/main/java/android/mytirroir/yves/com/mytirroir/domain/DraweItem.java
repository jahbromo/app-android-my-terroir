package android.mytirroir.yves.com.mytirroir.domain;

import android.graphics.drawable.Drawable;
import android.mytirroir.yves.com.mytirroir.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by absidibe on 21/12/15.
 */
public class DraweItem {

    private int icon;
    private String title;

    public DraweItem(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }
    public static List<DraweItem> items(){
        List<DraweItem> draweItems = new ArrayList<>();
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        draweItems.add(new DraweItem(R.drawable.artisanat,"Artisanat"));
        return draweItems;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }
}
