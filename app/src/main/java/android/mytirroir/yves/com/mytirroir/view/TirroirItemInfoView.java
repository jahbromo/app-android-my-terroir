package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import de.greenrobot.event.EventBus;

public class TirroirItemInfoView extends LinearLayout {
    private ImageView favoriImage;
    private TextView titleView;
    private TextView subTitleView;
    private TextView notView;
    private LocalDatabase localDatabase;
    private RatingBar ratingBar;

    public TirroirItemInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        localDatabase = new LocalDatabase(context);
        LayoutInflater.from(context).inflate(R.layout.tirroire_item_info, this, true);
        favoriImage = (ImageView) findViewById(R.id.img_favoris);
        titleView = (TextView) findViewById(R.id.textView_title);
        subTitleView = (TextView) findViewById(R.id.textView_subtitle);
        notView = (TextView) findViewById(R.id.textView_note_value);
        ratingBar = (RatingBar)findViewById(R.id.info_rating_bar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.title_color), PorterDuff.Mode.SRC_ATOP);
        int background = R.color.bg_transparent;
        TypedArray a = null;
        try {
            a = getContext().obtainStyledAttributes(attrs, R.styleable.overlay_background);
            setCustomBackground(background);
        } finally {
            if (a != null) {
                a.recycle();
            }
        }
    }

    private void updateFavIcon(boolean isFav) {
        favoriImage.setImageResource(isFav ? R.drawable.star_yellow : R.drawable.star_white);
    }

    public void setTerroir(final Terroir terroir) {
        boolean isFavori = localDatabase.isFavorite(terroir);
        updateFavIcon(isFavori);
        titleView.setText(terroir.getName());
        subTitleView.setText(terroir.getCity());
        notView.setText("3.6");

        favoriImage.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (localDatabase.isFavorite(terroir)) {
                        localDatabase.removeFavori(terroir);
                        updateFavIcon(false);
                    } else {
                        localDatabase.setFavori(terroir);
                        updateFavIcon(true);
                    }
                }
                return true;
            }
        });
    }

    public void setCustomBackground(int background) {
        this.setBackgroundResource(background);
        invalidate();

    }
}
