package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailHeaderView extends LinearLayout {

    private ImageView imageView;
    private  TextView textView;
    private ImageView favImageView;

    public DetailHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.detail_header_view, this, true);
        imageView = (ImageView) findViewById(R.id.detail_image);
        favImageView = (ImageView) findViewById(R.id.image_fav);
        textView = (TextView) findViewById(R.id.detail_description);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.detail_ratingbar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }

    private void updateFavIcon(boolean isFavori){
        favImageView.setImageResource(isFavori ? R.drawable.star_yellow : R.drawable.star_white);

    }
    public void reuse( final Terroir terroir) {
        Picasso.with(getContext()).load(terroir.getPhoto_detail()).fit().into(imageView);
        textView.setText(terroir.getDetail());
        final  LocalDatabase localDatabase = new LocalDatabase(getContext());
        boolean isFavori = localDatabase.isFavorite(terroir);
        updateFavIcon(isFavori);
        favImageView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (localDatabase.isFavorite(terroir)) {
                        localDatabase.removeFavori(terroir);
                        updateFavIcon(false);
                    } else {
                        localDatabase.setFavori(terroir);
                        updateFavIcon(true);
                    }
                }

                return true;
            }
        });

    }
}
