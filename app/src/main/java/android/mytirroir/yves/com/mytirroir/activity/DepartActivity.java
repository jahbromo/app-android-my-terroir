package android.mytirroir.yves.com.mytirroir.activity;

import android.content.Intent;
import android.graphics.Color;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.adapter.DepartementAdapter;
import android.mytirroir.yves.com.mytirroir.adapter.DrawerAdapter;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.mytirroir.yves.com.mytirroir.event.TerroirEvent;
import android.mytirroir.yves.com.mytirroir.network.RequestSender;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.util.PreferenceUtils;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

public class DepartActivity extends AppCompatActivity {

    public static final String AUTOMATIC_KEY = "auto";
    Switch aSwitch;
    ListView listView;
    private Departement selectedDepartement;
    List<Departement> departements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.departement_activivity);
        listView =(ListView)findViewById(R.id.departement_listView);
        departements =new LocalDatabase(this).loadDepartement();
        selectedDepartement = getIntent().getParcelableExtra(Constant.KEY_DEPARTEMENTS);
        selectedDepartement.setIsSelected(true);
        aSwitch = (Switch)findViewById(R.id.switch1);
        boolean isAutomatic = isAutomatic();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Département");
        aSwitch.setChecked(isAutomatic);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                Departement departement1 = (Departement) parent.getAdapter().getItem(position);
                DepartementEvent departementEvent = new DepartementEvent(departement1, null);
                intent.putExtra(Constant.KEY_DEPARTEMENTS, departementEvent);
                setResult(200, intent);
                finish();
            }
        });
        if(isAutomatic){
            Departement departement = PreferenceUtils.lastDepartementCode(DepartActivity.this).getDepartement();
            departement.setIsSelected(true);
            listView.setAdapter(new DepartementAdapter(departement));
        }else{
            departements.remove(selectedDepartement);
            departements.add(0,selectedDepartement);
            listView.setAdapter(new DepartementAdapter(departements));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Departement departement = PreferenceUtils.lastDepartementCode(DepartActivity.this).getDepartement();
                departement.setIsSelected(isChecked);
                if (isChecked) {
                    listView.setAdapter(new DepartementAdapter(departement));
                } else {
                    listView.setAdapter(new DepartementAdapter(departements));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void closeView(View view) {
        finish();
    }

    public boolean isAutomatic() {
        return this.selectedDepartement == null || PreferenceUtils.lastDepartementCode(this).getDepartement().equals(this.selectedDepartement);
    }
}
