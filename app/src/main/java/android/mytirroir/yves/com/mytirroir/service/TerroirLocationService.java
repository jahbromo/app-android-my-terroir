package android.mytirroir.yves.com.mytirroir.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import de.greenrobot.event.EventBus;


public class TerroirLocationService extends Service implements GoogleApiClient.ConnectionCallbacks, LocationListener {


    private GoogleApiClient mGoogleApiClient;
    private Location location;
    private float smallDeplacement = 30*1000;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        buildGoogleApiClient();
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
         location = getLastLocation();
        if(location != null) {
            onLocationChanged(location);
        }
        try{
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, createLocationRequest(), this);
        }catch (SecurityException e){
        }



    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setSmallestDisplacement(smallDeplacement);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location currentLocation) {
        GeocodingService.startGeocoding(this, currentLocation);

    }

    private Location getLastLocation(){
        try{
            LocationServices.FusedLocationApi.flushLocations(mGoogleApiClient);
            Location  location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(location != null){
                return location;
            }

        }catch (SecurityException e){

        }
        try{
            LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null){
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if(location == null){
                location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }
            return location;
        }catch (SecurityException e){
            return null;
        }


    }
}
