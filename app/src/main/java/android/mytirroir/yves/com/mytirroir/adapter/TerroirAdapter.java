package android.mytirroir.yves.com.mytirroir.adapter;

import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.view.ContactItemView;
import android.mytirroir.yves.com.mytirroir.view.TerroirItemView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class TerroirAdapter extends BaseAdapter {


    public TerroirAdapter(List<Terroir> detailItems) {
        this.detailItems = detailItems;
    }

    private List<Terroir> detailItems = new ArrayList<>();

    @Override
    public int getCount() {
        return detailItems.size();
    }

    @Override
    public Object getItem(int position) {
        return detailItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Terroir detailItem = detailItems.get(position);
        if (convertView != null && convertView instanceof TerroirItemView) {
            TerroirItemView itemView = (TerroirItemView) convertView;
            itemView.setTerror(detailItem);
            return itemView;
        }
        return new TerroirItemView(parent.getContext(), detailItem);
    }
}
