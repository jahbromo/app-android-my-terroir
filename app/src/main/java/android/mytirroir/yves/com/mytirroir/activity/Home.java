package android.mytirroir.yves.com.mytirroir.activity;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.adapter.ContactAdapter;
import android.mytirroir.yves.com.mytirroir.adapter.DrawerAdapter;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Category;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.mytirroir.yves.com.mytirroir.event.TerroirEvent;
import android.mytirroir.yves.com.mytirroir.event.TerroirSelectedEvent;
import android.mytirroir.yves.com.mytirroir.fragment.TerroirListFragment;
import android.mytirroir.yves.com.mytirroir.network.RequestSender;
import android.mytirroir.yves.com.mytirroir.service.GeocodingService;
import android.mytirroir.yves.com.mytirroir.service.TerroirLocationService;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.util.PreferenceUtils;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    public static final String KEY = "depart";
    private Toolbar toolbar;
    private List<Terroir> terroirs = new ArrayList<>();
    private TerroirListFragment listFragment;
    private NavigationView navigationView;
    private DataEvent dataEvent = new DataEvent();
    private DepartementEvent departementEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listFragment = (TerroirListFragment) getSupportFragmentManager().findFragmentById(R.id.fg_tirroir_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        EventBus.getDefault().register(this);
        refreshIcon(PreferenceUtils.lastDepartementCode(this));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY,departementEvent);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEventMainThread(DepartementEvent terroirEvent) {
        refreshIcon(terroirEvent);
    }

    @Subscribe
    public void onEvent(Terroir terroir){
        dataEvent.setCategory(null);
        dataEvent.setTerroirs(terroirs);
        dataEvent.setSelected(terroir);
        Constant.launchPoi(this, dataEvent);
    }
    private void filterContent(Category category) {
        dataEvent.setCategory(category);
        List<Terroir> results = new ArrayList<>();
        if (category == Category.ALL) {
            updateTerroir(terroirs);
            return;
        }
        if (category == Category.FAVORI) {
            if(departementEvent != null){
                results = new LocalDatabase(this).loadFavori(departementEvent);
            }else{
                results = new LocalDatabase(this).loadFavori(PreferenceUtils.lastDepartementCode(this));
            }
            updateTerroir(results);
            return;
        }
        for (Terroir terroir : terroirs) {
            if (terroir.getCategory() == category) {
                results.add(terroir);
            }
        }
        updateTerroir(results);
    }

    private void updateTerroir(List<Terroir> results) {
        if(results == null || results.isEmpty()){
            Toast.makeText(this,"Cette catégorie est vide", Toast.LENGTH_SHORT).show();
        }
        listFragment.setData(results);
        dataEvent.setTerroirs(results);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.main,menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchItem.setIcon(R.drawable.magnify);
        
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                List<Terroir> results =new ArrayList<Terroir>();
                for(Terroir terroir : terroirs){
                    if(terroir.isMatched(query)){
                        results.add(terroir);
                    }
                }
                listFragment.setData(results);
                dataEvent.setTerroirs(results);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Terroir> results =new ArrayList<Terroir>();
                for(Terroir terroir : terroirs){
                    if(terroir.isMatched(newText)){
                        results.add(terroir);
                    }
                }
                listFragment.setData(results);
                dataEvent.setTerroirs(results);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_map) {
            Intent intent = new Intent(this, MapList.class);
            dataEvent.setSelected(null);
            try{
                if(dataEvent.getTerroirs().isEmpty()){
                    dataEvent.setLocation(PreferenceUtils.lastDepartementCode(this).getLocation());
                }
            }catch (Exception e){

            }
            intent.putExtra(Constant.KEY_TERROIR, dataEvent);
            startActivity(intent);
            return true;
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Category category = null;

        if (id == R.id.nav_all) {
            category = Category.ALL;
        }
        if (id == R.id.nav_artisanat) {
            category = Category.ARTISANAT;
        }
        if (id == R.id.nav_patrimoine) {
            category = Category.PATRIMOINE;
        }
        if (id == R.id.nav_gatronomie) {
            category = Category.GASTRONOMIE;
        }
        if (id == R.id.nav_nature) {
            category = Category.NATURE;
        }
        if (id == R.id.nav_sauvegarde) {
            Toast.makeText(this,"Fonctionalite non implementee",Toast.LENGTH_SHORT).show();
        }
        if (id == R.id.nav_evenement) {
            category = Category.EVENEMENT;
        }
        if (id == R.id.nav_localisation) {
            Intent intent = new Intent(Home.this, DepartActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra(Constant.KEY_DEPARTEMENTS,getDepartement());
            startActivityForResult(intent, 200, bundle);
            return true;
        }
        filterContent(category);
        toolbar.setTitle(item.getTitle());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void refreshIcon(DepartementEvent event) {
        Departement departement = event.getDepartement();
        View view = navigationView.getHeaderView(0);
        navigationView.setItemIconTintList(ColorStateList.valueOf(getResources().getColor(R.color.title_color)));
        final ImageView imageView = (ImageView) view.findViewById(R.id.image_id);
        TextView textView = (TextView) view.findViewById(R.id.header_title);
        textView.setText(departement.getName());
        TextView subtextView = (TextView) view.findViewById(R.id.header_subtitle);
        subtextView.setText(departement.getPrefecture());
        getSupportActionBar().setTitle("Tous");
        Picasso.with(this).load(departement.getImage()).fit().into(imageView);
        dataEvent.setDepartementName(departement.present());
        terroirs = new LocalDatabase(this).loadLocalTerroir(event);
        listFragment.setData(terroirs);
        dataEvent.setTerroirs(terroirs);
        this.departementEvent = event;
        updateMenu();
    }

    private void updateMenu() {
        MenuItem menuItem = navigationView.getMenu().findItem(R.id.nav_localisation);
        final TextView actionView = (TextView)MenuItemCompat.getActionView(menuItem);
        actionView.setText(isAutomatic()? "En fonction de ma position":" ma selection");
    }

    private boolean isAutomatic() {
        return this.departementEvent == null || this.departementEvent.equals(PreferenceUtils.lastDepartementCode(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 200){
            DepartementEvent departement = data.getParcelableExtra(Constant.KEY_DEPARTEMENTS);
            refreshIcon(departement);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public Departement getDepartement() {
        if(departementEvent.getDepartement() !=null && departementEvent.getDepartement() != null){
           return departementEvent.getDepartement();
        }
        return PreferenceUtils.lastDepartementCode(this).getDepartement();
    }
}