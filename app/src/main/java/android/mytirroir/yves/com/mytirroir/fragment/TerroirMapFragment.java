package android.mytirroir.yves.com.mytirroir.fragment;

import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.activity.Detail;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.view.TirroirItemInfoView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;


public class TerroirMapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TirroirItemInfoView tirroirItemInfoView;
    private HashMap<Marker, Terroir> markerMap = new HashMap<>();
    private DataEvent dataEvent;

    public   static TerroirMapFragment  newInstance(DataEvent dataEvent){
        TerroirMapFragment fragment = new TerroirMapFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.KEY_TERROIR, dataEvent);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_list_fragment, container, false);
        tirroirItemInfoView = (TirroirItemInfoView) v.findViewById(R.id.view_info);
        tirroirItemInfoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Detail.class);
                intent.putExtra(Constant.KEY_TERROIR, dataEvent);
                getContext().startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dataEvent = getArguments().getParcelable(Constant.KEY_TERROIR);
        if(dataEvent.getSelected() != null){
            List<Terroir> terroirs = new ArrayList<>();
            terroirs.add(dataEvent.getSelected());
            dataEvent.setTerroirs(terroirs);
        }
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(dataEvent.getMapOptions());
        FragmentManager fm = getChildFragmentManager();
        fm.beginTransaction().replace(R.id.map_layout, supportMapFragment).commit();
        supportMapFragment.getMapAsync(this);
    }

    public void setSelectedTerroir(Terroir selectedTerroir){
        dataEvent.setSelected(selectedTerroir);
        for (Map.Entry mapentry : markerMap.entrySet()) {
            Marker currentMarker = (Marker)mapentry.getKey();
            Terroir terroirCurrent = (Terroir)mapentry.getValue();
            currentMarker.setIcon(BitmapDescriptorFactory.fromResource(terroirCurrent.getLogo()));
            if(terroirCurrent.getId() == selectedTerroir.getId()){
                currentMarker.setIcon(BitmapDescriptorFactory.fromResource(selectedTerroir.getSelectLogo()));
            }
        }
        centerOnMarker(selectedTerroir);
        EventBus.getDefault().post(new TitleEvent(selectedTerroir.getName()));
    }
    private  LatLngBounds.Builder latLngBounds;
    public void setData(List<Terroir> list) {
        mMap.clear();
        markerMap = new HashMap<>();
        latLngBounds = LatLngBounds.builder();
        for (Terroir tirroir : list) {
            latLngBounds.include(tirroir.getLatLng());
            Marker marker = addIcon(tirroir);
            markerMap.put(marker, tirroir);
        }
        try{
            if(!dataEvent.getTerroirs().isEmpty()){
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 9,300,300));
            }
        }catch (Exception data){

        }
        mMap.setPadding(100, 150, 70, 170);
    }

    public void centerOnMarker(final Terroir terroir){
        tirroirItemInfoView.setVisibility(View.VISIBLE);
        tirroirItemInfoView.setTerroir(terroir);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(terroir.getLatLng(), 9.0f));
    }

    private Marker addIcon(Terroir terroir) {
        MarkerOptions markerOptions = new MarkerOptions().snippet(terroir.getName()).
                icon(BitmapDescriptorFactory.fromResource(terroir.getLogo())).
                position(terroir.getLatLng());
        return mMap.addMarker(markerOptions);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        setData(dataEvent.getTerroirs());
        if(dataEvent.getSelected() !=null){
            setSelectedTerroir(dataEvent.getSelected());
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                setSelectedTerroir(markerMap.get(marker));
                return true;
            }
        });
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                setData(dataEvent.getTerroirs());
                if (dataEvent.getSelected() != null) {
                    setSelectedTerroir(dataEvent.getSelected());
                }
                try{
                    mMap.setMyLocationEnabled(true);
                }catch (Exception e){

                }
            }
        });
    }
}
