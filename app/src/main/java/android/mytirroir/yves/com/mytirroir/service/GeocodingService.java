package android.mytirroir.yves.com.mytirroir.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.mytirroir.yves.com.mytirroir.util.PreferenceUtils;
import android.os.Handler;
import android.os.Looper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;


public class GeocodingService extends IntentService {


    public GeocodingService() {
        super("GeocodingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Location location = intent.getParcelableExtra("location");
        Geocoder geocoder = new Geocoder(this);
        final List<Address> adresses = new ArrayList<>();
        try {
            adresses.addAll(geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (adresses == null || adresses.isEmpty()) {
            return;
        }
        String departementCode = adresses.get(0).getPostalCode();
        Departement departement = new LocalDatabase(this).getDepartement(departementCode);
        if(departement!=null){
            final DepartementEvent event = new DepartementEvent(departement,location);
            PreferenceUtils.saveCodeDepartement(this, event);
            Handler mHandler = new Handler(Looper.getMainLooper());
            if (Looper.myLooper() == Looper.getMainLooper()) {
                EventBus.getDefault().post(event);
            } else {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(event);
                    }
                });
            }
        }

    }

    public static void startGeocoding(Context context, Location code) {
        Intent intent = new Intent(context, GeocodingService.class);
        intent.putExtra("location", code);
        context.startService(intent);

    }
}
