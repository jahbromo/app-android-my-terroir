package android.mytirroir.yves.com.mytirroir.network;

import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataParseur {

    public List<Terroir> parse(String content) {
        try {
            Type collectionType = new TypeToken<List<Terroir>>() {
            }.getType();
            List<Terroir> results = new Gson().fromJson(content, collectionType);
            Collections.shuffle(results);
            return results;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<Terroir> parse(String content,DepartementEvent event) {
        List<Terroir> results =parse(content);
        List<Terroir> terroirs = new ArrayList<>();
        String codePostale = String.valueOf(event.getDepartement().getId());
        for(Terroir terroir : results){
            if(terroir.getCode().toLowerCase().startsWith(codePostale)){
                terroirs.add(terroir);
            }
        }
        return terroirs;
    }

    public List<Departement> parseDepartemnt(String string) {
        try {
            Type collectionType = new TypeToken<List<Departement>>() {
            }.getType();
            return new Gson().fromJson(string, collectionType);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
