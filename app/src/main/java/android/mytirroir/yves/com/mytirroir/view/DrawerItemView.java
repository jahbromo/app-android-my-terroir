package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.DraweItem;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DrawerItemView extends LinearLayout {
    private ImageView userPhoto;
    private TextView info;


    public DrawerItemView(Context context, DraweItem contact) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.drawer_item_layout, this, true);
        userPhoto = (ImageView) findViewById(R.id.drawer_item_image);
        info = (TextView) findViewById(R.id.drawer_item_tv);
        reuse(contact);
    }

    public void reuse(final DraweItem contact) {
        userPhoto.setImageResource(contact.getIcon());
        info.setText(contact.getTitle());

    }
}
