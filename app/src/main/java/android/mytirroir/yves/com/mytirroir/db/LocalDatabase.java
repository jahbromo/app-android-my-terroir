package android.mytirroir.yves.com.mytirroir.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.mytirroir.yves.com.mytirroir.network.DataParseur;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class LocalDatabase extends SQLiteOpenHelper {

    public LocalDatabase(Context context) {
        super(context, "tirroir_db", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PUSH_TABLE = "CREATE TABLE terroir_local (" +
                "content TEXT, date_added TEXT )";
        String CREATE_DEPARTEMENT_TABLE = "CREATE TABLE departement (" +
                "content TEXT, date_added TEXT )";
        String CREATE_FAVORI_TABLE = "CREATE TABLE terroir_favori ( terroir INTEGER PRIMARY KEY , date_added TEXT )";
        db.execSQL(CREATE_PUSH_TABLE);
        db.execSQL(CREATE_FAVORI_TABLE);
        db.execSQL(CREATE_DEPARTEMENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Terroir> loadLocalTerroir() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("terroir_local", null, null, null, null, null, null, null);
        if (cursor.moveToNext()) {
            return new DataParseur().parse(cursor.getString(0));
        }
        return new ArrayList<>();
    }
    public List<Terroir> loadLocalTerroir(DepartementEvent event) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("terroir_local", null, null, null, null, null, null, null);
        if (cursor.moveToNext()) {
            return new DataParseur().parse(cursor.getString(0),event);
        }
        return new ArrayList<>();
    }

    public List<Departement> loadDepartement() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("departement", null, null, null, null, null, null, null);
        if (cursor.moveToNext()) {
            return new DataParseur().parseDepartemnt(cursor.getString(0));
        }
        return new ArrayList<>();
    }


    public void saveTerroirContent(String content) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("terroir_local", null, null);
        ContentValues values = new ContentValues();
        values.put("content", content);
        values.put("date_added", String.valueOf(System.currentTimeMillis()));
        db.insert("terroir_local", null, values);
        db.close();
    }

    public void saveDepartementContent(String content) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("departement", null, null);
        ContentValues values = new ContentValues();
        values.put("content", content);
        values.put("date_added", String.valueOf(System.currentTimeMillis()));
        db.insert("departement", null, values);
        db.close();
    }

    public boolean isFavorite(Terroir terroir) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = "terroir  = ?";
        String[] selectionArgs = {String.valueOf(terroir.getId())};

        Cursor cursor = db.query("terroir_favori", null, selection, selectionArgs, null, null, null, null);
        if (cursor.moveToNext()) {
            return true;
        }
        return false;
    }

    public List<Terroir> loadFavori(DepartementEvent event) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("terroir_favori", null, null, null, null, null, null, null);
        List<Integer> ids = new ArrayList<>();
        while (cursor.moveToNext()) {
            ids.add(cursor.getInt(0));
        }
        List<Terroir> results = new ArrayList<>();
        List<Terroir> terroirs = loadLocalTerroir(event);
        for (Terroir item : terroirs) {
            if (ids.contains(item.getId())) {
                results.add(item);
            }
        }
        return results;
    }

    public void setFavori(Terroir terroir) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("terroir", terroir.getId());
            values.put("date_added", String.valueOf(System.currentTimeMillis()));
            db.insert("terroir_favori", null, values);
        } catch (Exception e) {

        }

    }

    public void removeFavori(Terroir terroir) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = "terroir  = ?";
        String[] selectionArgs = {String.valueOf(terroir.getId())};
        db.delete("terroir_favori", selection, selectionArgs);
    }

    public Departement getDepartement(String code) {
        if (code == null || code.length() < 2) {
            return null;
        }
        code = code.substring(0, 2);
        List<Departement> departements = loadDepartement();
        for (Departement departement : departements) {
            if (String.valueOf(departement.getId()).equalsIgnoreCase(code)) {
                return departement;
            }
        }
        return null;
    }

    public List<Terroir> loadLocalTerroir(String query) {

        List<Terroir> result = new ArrayList<>();
        List<Terroir> terroirs = loadLocalTerroir();
        if(query.trim().length()==0){
            return terroirs;
        }
        for(Terroir terroir : terroirs){
            if(terroir.isMatched(query)){
                result.add(terroir);
            }
        }
        return result;

    }
}
