package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DepartementItemView extends LinearLayout {
    private TextView info;
    private  ImageView imageView;
    private TextView textView;
    private ImageView selectedImageView;

    public DepartementItemView(Context context, Departement departement) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.departement_list_item_layout, this, true);
        info = (TextView) findViewById(R.id.tv_departement_item);
        imageView = (ImageView) findViewById(R.id.departement_image);
        textView = (TextView)findViewById(R.id.departement_info);
        selectedImageView = (ImageView) findViewById(R.id.icon_selected);
        reuse(departement);
    }

    public void reuse(final Departement contact) {
        info.setText(contact.getId() + " " + contact.getName());
        Picasso.with(getContext()).load(contact.getImage()).into(imageView);
        selectedImageView.setVisibility(contact.isSelected() ?VISIBLE:INVISIBLE);
        textView.setText(contact.info());
    }
}
