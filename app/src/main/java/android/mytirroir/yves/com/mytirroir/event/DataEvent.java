package android.mytirroir.yves.com.mytirroir.event;

import android.location.Location;
import android.mytirroir.yves.com.mytirroir.domain.Category;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;

public class DataEvent implements Parcelable {

    private Category category;
    private List<Terroir> terroirs = new ArrayList<>();
    private Terroir selected;
    private String departementName;
    private Location location;

    public DataEvent() {
    }

    public String getDepartementName() {
        return departementName;
    }

    public void setDepartementName(String departementName) {
        this.departementName = departementName;
    }

    public static Creator<DataEvent> getCREATOR() {
        return CREATOR;
    }

    public LatLng getLocation() {
        return  new LatLng(location.getLatitude(),location.getLongitude());
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public DataEvent(Category category, List<Terroir> terroirs) {
        this.category = category;
        this.terroirs = terroirs;
    }
    public DataEvent(Terroir selected, List<Terroir> terroirs) {
        this.selected = selected;
        this.terroirs = terroirs;
    }

    public Terroir getSelected() {
        return selected;
    }

    public void setSelected(Terroir selected) {
        this.selected = selected;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Terroir> getTerroirs() {
        return terroirs;
    }

    public void setTerroirs(List<Terroir> terroirs) {
        this.terroirs = terroirs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.category == null ? -1 : this.category.ordinal());
        dest.writeTypedList(terroirs);
        dest.writeParcelable(this.selected, 0);
        dest.writeString(this.departementName);
        dest.writeParcelable(this.location, 0);
    }

    protected DataEvent(Parcel in) {
        int tmpCategory = in.readInt();
        this.category = tmpCategory == -1 ? null : Category.values()[tmpCategory];
        this.terroirs = in.createTypedArrayList(Terroir.CREATOR);
        this.selected = in.readParcelable(Terroir.class.getClassLoader());
        this.departementName = in.readString();
        this.location = in.readParcelable(Location.class.getClassLoader());
    }

    public static final Creator<DataEvent> CREATOR = new Creator<DataEvent>() {
        public DataEvent createFromParcel(Parcel source) {
            return new DataEvent(source);
        }

        public DataEvent[] newArray(int size) {
            return new DataEvent[size];
        }
    };

    public GoogleMapOptions getMapOptions() {
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if(selected != null){
            googleMapOptions.camera(CameraPosition.fromLatLngZoom(selected.getLatLng(),10));
        }else{
            final LatLngBounds.Builder builder = LatLngBounds.builder();
            for (Terroir tirroir : terroirs) {
                builder.include(tirroir.getLatLng());
            }
            try{
                if(location!=null){
                    builder.include(new LatLng(this.location.getLatitude(),this.location.getLongitude()));
                }
            }catch (Exception e){
            }

            try{
                googleMapOptions.camera(CameraPosition.fromLatLngZoom(builder.build().getCenter(), 10));
            }catch (Exception e){

            }
        }

        return googleMapOptions;
    }
}
