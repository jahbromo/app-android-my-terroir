package android.mytirroir.yves.com.mytirroir.fragment;

import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.adapter.TerroirAdapter;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;


public class TerroirListFragment extends Fragment {

    private ListView rv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tirroir_list_fragment, container, false);
        rv = (ListView) v.findViewById(R.id.rv_list_tirroir);
        TextView empty = (TextView)v.findViewById(R.id.empty_list_view);
        rv.setEmptyView(empty);
        return v;
    }

    public void setData(List<Terroir> data) {
        TerroirAdapter tirroirAdapter = new TerroirAdapter(data);
        rv.setAdapter(tirroirAdapter);
    }
}
