package android.mytirroir.yves.com.mytirroir.fragment;

/**
 * Created by absidibe on 23/12/15.
 */
public class TitleEvent {
    private String name;
    public TitleEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
