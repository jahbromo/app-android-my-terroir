package android.mytirroir.yves.com.mytirroir.network;

import android.content.Context;
import android.mytirroir.yves.com.mytirroir.activity.Main;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.TerroirEvent;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


public class RequestSender {

    private static final String BASE_URL = "http://107.170.44.242";
    RequestService service;

    public RequestSender() {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BASE_URL).setConverter(new GsonConverter(new Gson())).setLogLevel(RestAdapter.LogLevel.FULL).build();
        service = restAdapter.create(RequestService.class);
    }

    public void loadTerroirs(final Context context) {

        service.listTerroirs(new Callback<List<Terroir>>() {
            @Override
            public void success(List<Terroir> terroirs, Response response) {
                TerroirEvent terroirEvent = new TerroirEvent(terroirs);
                terroirEvent.setRaw(convert(response));
                LocalDatabase localDatabase = new LocalDatabase(context);
                localDatabase.saveTerroirContent(terroirEvent.getRaw());
                EventBus.getDefault().post(terroirEvent);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                EventBus.getDefault().post(new TerroirEvent(true));
            }
        });

    }

    public void loadDepartement(final Context context) {

        service.listDepartement(new Callback<List<Departement>>() {
            @Override
            public void success(List<Departement> departements, Response response) {
                LocalDatabase localDatabase = new LocalDatabase(context);
                localDatabase.saveDepartementContent(convert(response));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private String convert(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {

            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void load(Context context) {
        loadTerroirs(context);
        loadDepartement(context);
    }
}
