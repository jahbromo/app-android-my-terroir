package android.mytirroir.yves.com.mytirroir.event;

import android.mytirroir.yves.com.mytirroir.domain.Terroir;

import java.util.List;

/**
 * Created by absidibe on 28/11/15.
 */
public class TerroirEvent {

    private List<Terroir> terroirs;
    private String raw;
    private boolean error;

    public TerroirEvent(List<Terroir> terroirs) {
        this.terroirs = terroirs;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public TerroirEvent(boolean error) {
        this.error = error;
    }

    public List<Terroir> getTerroirs() {
        return terroirs;
    }

    public String getRaw() {
        return raw;
    }

    public boolean isError() {
        return error;
    }
}
