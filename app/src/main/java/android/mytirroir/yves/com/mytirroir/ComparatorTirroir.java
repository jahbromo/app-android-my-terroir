package android.mytirroir.yves.com.mytirroir;

import android.mytirroir.yves.com.mytirroir.domain.Terroir;

import java.util.Comparator;


public class ComparatorTirroir implements Comparator<Terroir> {
    @Override
    public int compare(Terroir lhs, Terroir rhs) {
        if(lhs == null){
            return 1;
        }
        if(rhs == null){
            return -1;
        }
        return Double.compare(lhs.getDistance(),rhs.getDistance());
    }
}
