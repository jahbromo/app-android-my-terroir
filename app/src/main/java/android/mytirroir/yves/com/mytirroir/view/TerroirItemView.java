package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.util.DistanceFormatter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class TerroirItemView extends LinearLayout{

    private TirroirItemInfoView infoView;
    private ImageView imageView;
    private TextView distanceView;


    public TerroirItemView(Context context, Terroir terroir) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.tirroire_layout_item, this, true);
        imageView = (ImageView) findViewById(R.id.img_list);
        distanceView = (TextView) findViewById(R.id.tv_distance);
        infoView = (TirroirItemInfoView) findViewById(R.id.view_info);
        setTerror(terroir);
    }

    public void setTerror(final Terroir terror) {
        if(terror.getDistance()>1){
            distanceView.setVisibility(View.VISIBLE);
            distanceView.setText(DistanceFormatter.formatDistance(terror.getDistance()));
        }else{
            distanceView.setVisibility(View.GONE);
        }
        infoView.setTerroir(terror);
        Picasso.with(getContext()).load(terror.getPhoto_list()).into(imageView);
        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(terror);
            }
        });
    }
}
