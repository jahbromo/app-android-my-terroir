package android.mytirroir.yves.com.mytirroir.domain;


import android.location.Location;
import android.mytirroir.yves.com.mytirroir.event.DepartementEvent;
import android.os.Parcel;
import android.os.Parcelable;

public class Departement implements Parcelable {

    private int id;
    private String name;
    private String prefecture;
    private String region;
    private String image;
    private boolean isSelected;

    public int getId() {
        return id;
    }

    public static DepartementEvent getDefault() {
        Departement departement = new Departement();
        departement.id = 82;
        departement.name = "Tarn et Garonne";
        departement.prefecture = "Un patrimoine naturel et culturel d'exception";
        departement.region = "Midi pyrénées Languedoc Roussillon";
        departement.image = "http://www.meteocity.com/medias/gallery/large/france/REG14/DPTM82/paysage-tarn-et-garonne.jpg";
        Location location = new Location("garonne");
        location.setLongitude(1.351379);
        location.setLatitude(44.091718);
        return new DepartementEvent(departement,location);
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefecture() {
        return prefecture;
    }

    public void setPrefecture(String prefecture) {
        this.prefecture = prefecture;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "DepartActivity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", prefecture='" + prefecture + '\'' +
                ", region='" + region + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public String present() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.prefecture);
        dest.writeString(this.region);
        dest.writeString(this.image);
    }

    public Departement() {
    }

    protected Departement(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.prefecture = in.readString();
        this.region = in.readString();
        this.image = in.readString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Departement that = (Departement) o;

        if (id != that.id) return false;
        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }

    public static final Parcelable.Creator<Departement> CREATOR = new Parcelable.Creator<Departement>() {
        public Departement createFromParcel(Parcel source) {
            return new Departement(source);
        }

        public Departement[] newArray(int size) {
            return new Departement[size];
        }
    };

    public String info() {
        return +this.id+"000, "+ prefecture +", "+ region;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
