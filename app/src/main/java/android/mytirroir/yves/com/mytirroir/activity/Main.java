package android.mytirroir.yves.com.mytirroir.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.TerroirEvent;
import android.mytirroir.yves.com.mytirroir.network.RequestSender;
import android.mytirroir.yves.com.mytirroir.service.TerroirLocationService;
import android.os.Bundle;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

public class Main extends Activity {

    private static final int LOCATION_REQUEST_CODE = 1212;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.loading_page);
        grantPermission();
        EventBus.getDefault().register(this);
    }

    private void startLoad(){
        List<Terroir> terroirs = new LocalDatabase(this).loadLocalTerroir();
        if (terroirs == null || terroirs.isEmpty()) {
            new RequestSender().load(this);
        } else {
            startActivity(new Intent(this, Home.class));
        }
    }
    @Subscribe
    public void onEventMainThread(TerroirEvent terroirEvent) {
        if (terroirEvent.isError()) {
            Toast.makeText(this, "Error loading Terroirs", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, Home.class));
        } else {
            startActivity(new Intent(this, Home.class));
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void grantPermission() {
        if(PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},LOCATION_REQUEST_CODE);
        }else{
            startLoad();
            startService(new Intent(this, TerroirLocationService.class));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService(new Intent(this, TerroirLocationService.class));
                }else{
                    Toast.makeText(this,"La localisation est necessaire",Toast.LENGTH_LONG).show();
                }
                startLoad();
            }
            return;
        }
    }
}
