package android.mytirroir.yves.com.mytirroir.network;

import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;


public interface RequestService {

    @GET("/api/tirroir/")
    void listTerroirs(Callback<List<Terroir>> callback);

    @GET("/api/departement/")
    void listDepartement(Callback<List<Departement>> callback);
}
