package android.mytirroir.yves.com.mytirroir.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.event.TerroirSelectedEvent;
import android.mytirroir.yves.com.mytirroir.fragment.TerroirMapDetailFragment;
import android.mytirroir.yves.com.mytirroir.fragment.TitleEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.mytirroir.yves.com.mytirroir.view.TirroirItemInfoView;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;


public class MapPoi extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private TerroirSelectedEvent selectedEvent;
    private static final String KEY_INSTANCE_SAVED = "key_saved";
    private GoogleMap googleMap;
    private TirroirItemInfoView itemInfoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.map_poi_activivity_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_back);
        if (savedInstanceState != null) {
            selectedEvent = savedInstanceState.getParcelable(KEY_INSTANCE_SAVED);
        }
        if (selectedEvent == null) {
            selectedEvent = getIntent().getParcelableExtra(Constant.KEY_SELECTED);
        }
        toolbar.setTitle(selectedEvent.getTerroir().getName());
        initMap();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // itemInfoView = (TirroirItemInfoView)findViewById(R.id.view_info);
       // itemInfoView.setTerroir(selectedEvent.getTerroir());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_INSTANCE_SAVED, selectedEvent);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_poi_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        if (id == R.id.action_direction) {
           Constant.launchDirection(this,selectedEvent.getTerroir());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapLoaded() {
        setupMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setupMap();

    }
    private void initMap(){
        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(selectedEvent.getMapOptions());
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.map_poi_layout, supportMapFragment).commit();
        supportMapFragment.getMapAsync(this);
    }
    private void setupMap(){
        try {
            this.googleMap.setOnMapLoadedCallback(this);
            this.googleMap.setPadding(50, 50, 50, 50);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            this.googleMap.setMyLocationEnabled(true);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedEvent.getTerroir().getLatLng(), 10.0f));
        }
        catch(Exception e){

        }
    }
}
