package android.mytirroir.yves.com.mytirroir.domain;

import android.mytirroir.yves.com.mytirroir.util.Constant;

/**
 * Created by absidibe on 10/12/15.
 */
public class Comment {

    private String photo;
    private String name;
    private long date;
    private String text;

    public Comment(String photo, String name, long date, String text) {
        this.photo = photo;
        this.name = name;
        this.date = date;
        this.text = text;
    }

    public Comment() {
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Comment create() {
        Comment comment = new Comment();
        comment.date = System.currentTimeMillis() - 1000000;
        comment.name = "Sebastien Dupont";
        comment.text = Constant.TEXT;
        comment.photo = "https://lh4.googleusercontent.com/-Rr58i2SCIiQ/AAAAAAAAAAI/AAAAAAAAMRg/903oZUlcwPA/photo.jpg?sz=40";
        return comment;
    }
}
