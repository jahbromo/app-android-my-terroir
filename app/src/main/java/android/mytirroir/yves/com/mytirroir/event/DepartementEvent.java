package android.mytirroir.yves.com.mytirroir.event;

import android.location.Location;
import android.mytirroir.yves.com.mytirroir.domain.Departement;
import android.os.Parcel;
import android.os.Parcelable;


public class DepartementEvent implements Parcelable {

    private Location location;
    private Departement departement;

    public DepartementEvent(Departement departement, Location location) {
        this.departement = departement;
        this.location = location;
    }

    public Departement getDepartement() {
        return departement;

    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.location, 0);
        dest.writeParcelable(this.departement, 0);
    }

    protected DepartementEvent(Parcel in) {
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.departement = in.readParcelable(Departement.class.getClassLoader());
    }

    public static final Parcelable.Creator<DepartementEvent> CREATOR = new Parcelable.Creator<DepartementEvent>() {
        public DepartementEvent createFromParcel(Parcel source) {
            return new DepartementEvent(source);
        }

        public DepartementEvent[] newArray(int size) {
            return new DepartementEvent[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartementEvent that = (DepartementEvent) o;

        return departement.equals(that.departement);

    }

    @Override
    public int hashCode() {
        return departement.hashCode();
    }
}
