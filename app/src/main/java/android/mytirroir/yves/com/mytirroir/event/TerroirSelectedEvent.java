package android.mytirroir.yves.com.mytirroir.event;

import android.location.Location;
import android.mytirroir.yves.com.mytirroir.domain.Category;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

public class TerroirSelectedEvent implements Parcelable {

    private Terroir terroir;


    public TerroirSelectedEvent(Terroir terroir) {
        this.terroir = terroir;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.terroir, 0);
    }

    public Terroir getTerroir() {
        return terroir;
    }

    protected TerroirSelectedEvent(Parcel in) {
        this.terroir = in.readParcelable(Terroir.class.getClassLoader());
    }

    public static final Parcelable.Creator<TerroirSelectedEvent> CREATOR = new Parcelable.Creator<TerroirSelectedEvent>() {
        public TerroirSelectedEvent createFromParcel(Parcel source) {
            return new TerroirSelectedEvent(source);
        }

        public TerroirSelectedEvent[] newArray(int size) {
            return new TerroirSelectedEvent[size];
        }
    };
    public GoogleMapOptions getMapOptions() {
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if(terroir != null){
            googleMapOptions.camera(CameraPosition.fromLatLngZoom(terroir.getLatLng(),10));
        }
        return googleMapOptions;
    }
}
