package android.mytirroir.yves.com.mytirroir.adapter;

import android.mytirroir.yves.com.mytirroir.domain.Comment;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.view.CommentItemView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends BaseAdapter {

    public CommentAdapter(Terroir terroir) {
        this.comments = terroir.comments();
    }

    private List<Comment> comments = new ArrayList<>();

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Comment comment = comments.get(position);
        if (convertView != null && convertView instanceof CommentItemView) {
            CommentItemView itemView = (CommentItemView) convertView;
            itemView.reuse(comment);
            return itemView;
        }
        return new CommentItemView(parent.getContext(), comment);
    }
}
