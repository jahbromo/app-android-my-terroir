package android.mytirroir.yves.com.mytirroir.adapter;

import android.mytirroir.yves.com.mytirroir.domain.DraweItem;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.view.DrawerItemView;
import android.mytirroir.yves.com.mytirroir.view.TerroirItemView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class DrawerAdapter extends BaseAdapter {


    public DrawerAdapter(boolean enable) {
        if(enable){
            this.detailItems = DraweItem.items();
        }else{
            this.detailItems = new ArrayList<>();
        }
    }

    private List<DraweItem> detailItems = new ArrayList<>();

    @Override
    public int getCount() {
        return detailItems.size();
    }

    @Override
    public Object getItem(int position) {
        return detailItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DraweItem detailItem = detailItems.get(position);
        if (convertView != null && convertView instanceof DrawerItemView) {
            DrawerItemView itemView = (DrawerItemView) convertView;
            itemView.reuse(detailItem);
            return itemView;
        }
        return new DrawerItemView(parent.getContext(), detailItem);
    }
}
