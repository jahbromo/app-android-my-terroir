package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.content.Intent;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.net.Uri;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ContactItemView extends LinearLayout {
    private ImageView userPhoto;
    private TextView info;


    public ContactItemView(Context context, Contact contact) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.detail_item_layout, this, true);
        userPhoto = (ImageView) findViewById(R.id.detail_item_image);
        info = (TextView) findViewById(R.id.detail_item_tv);
        reuse(contact);
    }

    public void reuse(final Contact contact) {
        userPhoto.setImageResource(contact.getIcon());
        info.setText(contact.getTerroir());
        info.setAutoLinkMask(Linkify.ALL);
        info.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact.getAction() == 3) {
                    try {
                        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(contact.getTerroir()));
                        getContext().startActivity(launchBrowser);

                    } catch (Exception e) {

                    }
                }
                if (contact.getAction() == 1) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + contact.getTerroir()));
                        getContext().startActivity(intent);
                    } catch (Exception e) {

                    }
                }
            }
        });
    }
}
