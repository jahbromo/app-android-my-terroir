package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Comment;
import android.mytirroir.yves.com.mytirroir.util.CircleTransform;
import android.mytirroir.yves.com.mytirroir.util.DistanceFormatter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CommentItemView extends LinearLayout {
    private ImageView userPhoto;
    private TextView username;
    private TextView cmtDate;
    private TextView commentContent;

    public CommentItemView(Context context, Comment comment) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.comment_layout_item, this, true);
        userPhoto = (ImageView) findViewById(R.id.cmt_image);
        username = (TextView) findViewById(R.id.tv_username);
        cmtDate = (TextView) findViewById(R.id.tv_date);
        commentContent = (TextView) findViewById(R.id.tv_content);
        reuse(comment);
    }

    public void reuse(Comment comment) {
        Picasso.with(getContext()).load(comment.getPhoto()).resize(50,50).transform(new CircleTransform()).into(userPhoto);
        username.setText(comment.getName());
        cmtDate.setText(DistanceFormatter.formateDate(comment.getDate()));
        commentContent.setText(comment.getText());
    }
}
