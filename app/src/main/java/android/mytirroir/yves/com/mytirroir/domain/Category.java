package android.mytirroir.yves.com.mytirroir.domain;

public enum Category {
    NATURE("Nature"),
    ARTISANAT("Artisanat"),
    GASTRONOMIE("Gastronomie"),
    PATRIMOINE("patrimoine"),
    ALL("Tous"),
    FAVORI("Favori"),
    LOCATION("Localisation"),
    EVENEMENT("Evenement");

    Category(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
