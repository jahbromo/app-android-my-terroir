package android.mytirroir.yves.com.mytirroir.view;

import android.content.Context;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.domain.Comment;
import android.mytirroir.yves.com.mytirroir.domain.Contact;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class DetailInfoView extends LinearLayout {
  
    private  LinearLayout listView;
    private  LinearLayout cmts;

    public DetailInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.detail_info_view, this, true);
        listView = ((LinearLayout) findViewById(R.id.detail_listview_info));
        cmts = ((LinearLayout) findViewById(R.id.detail_listview_comments));
        cmts.setOrientation(VERTICAL);
    }

    public void reuse(Terroir terroir) {
        for (Contact contact : terroir.detailItems()) {
            listView.addView(new ContactItemView(getContext(), contact));
        }

        for (Comment cmt : terroir.comments()) {
            cmts.addView(new CommentItemView(getContext(), cmt));
        }
    }
}
