package android.mytirroir.yves.com.mytirroir.activity;

import android.app.SearchManager;
import android.content.Context;
import android.mytirroir.yves.com.mytirroir.R;
import android.mytirroir.yves.com.mytirroir.db.LocalDatabase;
import android.mytirroir.yves.com.mytirroir.domain.Category;
import android.mytirroir.yves.com.mytirroir.domain.Terroir;
import android.mytirroir.yves.com.mytirroir.event.DataEvent;
import android.mytirroir.yves.com.mytirroir.fragment.TerroirMapFragment;
import android.mytirroir.yves.com.mytirroir.fragment.TitleEvent;
import android.mytirroir.yves.com.mytirroir.util.Constant;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;


public class MapList extends AppCompatActivity {

    private TerroirMapFragment mapFragment;
    private  DataEvent dataEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.map_activity);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.menu_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState !=null){
            dataEvent = savedInstanceState.getParcelable("save");
        }
        if(dataEvent == null){
            dataEvent =  getIntent().getParcelableExtra(Constant.KEY_TERROIR);
        }
        mapFragment = TerroirMapFragment.newInstance(dataEvent);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.activity_map_layout,mapFragment).commit();
        if(dataEvent.getCategory() != null){
            setTitle(dataEvent.getCategory().getName());
            return;
        }else{
            setTitle(Category.ALL.getName());
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("save", dataEvent);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        dataEvent = savedInstanceState.getParcelable("save");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe
    public void onEvent(TitleEvent title){
        setTitle(title.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                List<Terroir> terroirs = new LocalDatabase(MapList.this).loadLocalTerroir(query);
                mapFragment.setData(terroirs);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Terroir> terroirs = new LocalDatabase(MapList.this).loadLocalTerroir(newText);
                mapFragment.setData(terroirs);
                return false;
            }
        });

        return true;
    }
}
